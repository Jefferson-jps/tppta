console.log("Hello World");
//alert("Passou aqui - chamou a fun~]ao gravar");
/*
comentário
*/
let nome = "Gobbato";
let idade = 40;
let isProfessor = true;
let salario = 15242.14;

console.log(typeof(nome));
console.log(typeof(idade));
console.log(typeof(isProfessor));

console.log("2" + isProfessor);

console.log("A soma de 2 + 2 é " + (2 + 2));

console.log(`A soma de 2 + 2 é ${2+2}`);

//Funções

/*
console.log("Alguma coisa");
alert("Alguma coisa"); */

function escreverNoConsole() {
    console.log("Olá");
}

escreverNoConsole(); //chamada da função

function podeDirigir(idade, cnh) {
    if (idade >= 18 && cnh == true) {
        console.log("Pode dirigir");
    } else {
        console.log("Não pode dirigir");
    }
}

podeDirigir(18, true);
podeDirigir(16, true);

//Arrow Function

const parOuImpar = (valor) => {
    if (valor % 2 === 0) {

        console.log(`O número ${valor} é Par!`);
    } else {

        console.log(`O número ${valor} é Ímpar!`);

    }
}

parOuImpar(99);



/*
for (let x = 1; x <= 10; x++) {
    console.log("O valor de x é: " + x);
} 
*/

//Array

let numeros = [1, 3, 5, 8, 12];

console.log(numeros);

console.log(numeros.length);

console.log(numeros[2]);

let peixes = ["acara-bandeira", "palhaço", "mandarim", "esturjão"];

remover_ultimo = peixes.pop; //pop = retira o ultimo elemento
console.log(peixes);

adicionar = peixes.push("Peixe-espada");

console.log(peixes);

//Classes

const aluno = {
    nome: "Alexander",
    sobrenome: "Gobatto"

}

let aluno = {
    nome: '',
    setNome: function(vNome) {
        this.nome = vNome;
    },
    getNome: function() {
        return this.nome;
    }
}

class Pessoa {
    constructor(nome) {
        this.nome = nome;
    }
    getNome() {
        return this.nome;
    }
    setNome(value) {
        this.nome = value;
    }
}